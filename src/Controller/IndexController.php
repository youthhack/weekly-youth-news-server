<?php

namespace App\Controller;

use App\Repository\EditionRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    /**
     * @Route("/", name="index")
     */
    public function showIndexPage(EditionRepository $editionRepository): Response
    {
        $editions = $editionRepository->findAll();

        return $this->render('index.html.twig', [
            'message' => 'Hello World!',
            'editions' => $editions,
        ]);
    }
}
