<?php

namespace App\Controller\Admin;

use App\Entity\Edition;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

class DashboardController extends AbstractDashboardController
{
    private $translator;

    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;

        // Hier können fehlende Translations kurz gesetzt werden, damit sie mit
        // php bin/console translation:update --force de --domain messages
        // in die translations/messages.de.xlf Datei aufgenommen werden können
        // Beispiel:
        // $this->translator->trans('Weekly Youth News Server');
    }

    /**
     * @Route("/admin", name="admin")
     */
    public function index(): Response
    {
        return $this->render('admin/dashboard.html.twig');
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Weekly Youth News Server');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-dashboard');
        // yield MenuItem::section('Ausgaben');
        yield MenuItem::linkToCrud('Editions', 'fa fa-newspaper', Edition::class);
        // links to a different CRUD action
        yield MenuItem::linkToCrud('Add Edtion', 'fa fa-plus', Edition::class)
            ->setAction('new');
        yield MenuItem::linktoRoute('Adminbereich verlassen', 'fa fa-home', 'index');
    }
}
