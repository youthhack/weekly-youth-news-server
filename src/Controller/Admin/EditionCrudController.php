<?php

namespace App\Controller\Admin;

use App\Entity\Edition;
use DateTimeImmutable;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;

class EditionCrudController extends AbstractCrudController
{
    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            // the names of the Doctrine entity properties where the search is made on
            // (by default it looks for in all properties)
            ->setSearchFields(['content'])
            // use dots (e.g. 'seller.email') to search in Doctrine associations
            // ->setSearchFields(['name', 'description', 'seller.email', 'seller.phone'])
            // set it to null to disable and hide the search box
            // ->setSearchFields(null)

            // defines the initial sorting applied to the list of entities
            // (user can later change this sorting by clicking on the table columns)
            // ->setDefaultSort(['publishDatetime' => 'DESC'])
            ->setDefaultSort(['publishDatetime' => 'DESC', 'content' => 'ASC'])

            // the max number of entities to display per page
            ->setPaginatorPageSize(30)
            // these are advanced options related to Doctrine Pagination
            // (see https://www.doctrine-project.org/projects/doctrine-orm/en/2.7/tutorials/pagination.html)
            // ->setPaginatorUseOutputWalkers(true)
            // ->setPaginatorFetchJoinCollection(true)
        ;
    }

    public static function getEntityFqcn(): string
    {
        return Edition::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [
            DateTimeField::new('publishDatetime'),
            TextareaField::new('content'),
        ];
    }

    public function createEntity(string $entityFqcn)
    {
        $dateTime = new DateTimeImmutable('Friday next week 7:00');

        return new Edition('', $dateTime);
    }
}
