<?php

namespace App\Entity;

use App\Repository\EditionRepository;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity(repositoryClass=EditionRepository::class)
 */
class Edition
{
    /**
     * @ORM\Id
     * @ORM\Column(type="uuid")
     */
    private $uuid;

    /**
     * @ORM\Column(type="datetimetz_immutable")
     */
    private $publishDatetime;

    /**
     * @ORM\Column(type="text")
     */
    private $content;

    public function __construct(string $content, DateTimeInterface $publishAt)
    {
        if (! $publishAt instanceof DateTimeImmutable) {
            $publishAt = DateTimeImmutable::createFromMutable($publishAt);
        }

        $this->uuid = Uuid::uuid4();
        $this->publishDatetime = $publishAt;
        $this->content = $content;
    }

    public function getUuid(): string
    {
        return (string) $this->uuid;
    }

    public function getPublishDatetime(): DateTimeImmutable
    {
        return $this->publishDatetime;
    }

    public function setPublishDatetime(DateTimeInterface $publishAt): void
    {
        if (! $publishAt instanceof DateTimeImmutable) {
            $publishAt = DateTimeImmutable::createFromMutable($publishAt);
        }

        $this->publishDatetime = $publishAt;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }
}
