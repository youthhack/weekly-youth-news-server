# Weekly Youth News Server

Server für die Weekly Youth News der Jugend Frankenstraße in Düren.

## Development

### Übersetzung

Um neue Wörter übersetzen zu können, müssen die Wörter erst in die Datei `translations/messages.de.xlf` exportiert werden und der Cache gelöscht werden. Das geht mit diesem Befehl:

```
php bin/console translation:update --force de --domain messages
php bin/console cache:clear
```
